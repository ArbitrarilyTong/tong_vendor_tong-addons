# Copy XMLS
PRODUCT_COPY_FILES += \
    ArbitrarilyTong/Prebuilt_Addons/Moto/etc/motorola/bgintents/com.android.contacts.autorun.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/motorola/bgintents/com.android.contacts.autorun.xml \
    ArbitrarilyTong/Prebuilt_Addons/Moto/etc/motorola/bgintents/com.android.dialer.bgintents.xml:$(TARGET_COPY_OUT_PRODUCT)com.android.dialer.bgintents.xml \
    ArbitrarilyTong/Prebuilt_Addons/Moto/etc/motorola/bgintents/com.android.messaging.bgintents.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/motorola/bgintents/com.android.messaging.bgintents.xml \
    ArbitrarilyTong/Prebuilt_Addons/Moto/etc/permissions/privapp-permissions-com.android.contacts.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/privapp-permissions-com.android.contacts.xml \
    ArbitrarilyTong/Prebuilt_Addons/Moto/etc/permissions/privapp-permissions-com.android.dialer.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/privapp-permissions-com.android.dialer.xml \
    ArbitrarilyTong/Prebuilt_Addons/Moto/etc/permissions/privapp-permissions-com.android.messaging.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/privapp-permissions-com.android.messaging.xml \
    ArbitrarilyTong/Prebuilt_Addons/Moto/etc/sysconfig/hiddenapi-whitelist-com.android.prc.dialer.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/hiddenapi-whitelist-com.android.prc.dialer.xml \
    ArbitrarilyTong/Prebuilt_Addons/Moto/etc/sysconfig/hiddenapi-whitelist-com.android.prc.messaging.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/hiddenapi-whitelist-com.android.prc.messaging.xml

# Import Addons
PRODUCT_PACKAGES += \
    MotorolaCalculator \
    MotorolaClock \
    MotorolaCompass \
    MotorolaGallery \
    MotorolaStylus \
    MotorolaWidgets \
    MotorolaCalendar \
    MotorolaContacts \
    MotorolaDialer \
    MotorolaMessaging