# ArbitrarilyTong Apps
PRODUCT_PACKAGES += ArbitrarilyTongSettings

# Add GAPPS
$(call inherit-product-if-exists, vendor/gapps/common.mk)

# Add Prebuilt Apps
$(call inherit-product, ArbitrarilyTong/Prebuilt_Addons/PrebuiltAddons.mk)

# Add Prebuilt Apps from Github
$(call inherit-product, ArbitrarilyTong/Prebuilt_Addons/Github/PrebuiltApps.mk)

# Add Audio
$(call inherit-product, ArbitrarilyTong/Audio/audio.mk)

# Add Fonts
$(call inherit-product, ArbitrarilyTong/Fonts/font.mk)
