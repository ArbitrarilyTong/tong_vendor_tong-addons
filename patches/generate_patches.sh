#!/bin/bash

# 获取脚本所在目录
script_dir="$(readlink -f -- $(dirname "$0"))"
echo "脚本所在目录：$script_dir"

# 获取命令行执行所在目录
current_dir=$(pwd)
echo "命令行执行所在目录：$current_dir"

while read -r item; do
    repo_dir=$(jq -r '.path' <<< "$item")
    commits=$(jq -r '.commits[]' <<< "$item")

    # 创建对应路径下的目录
    mkdir -p "$script_dir/$repo_dir"
    # 计算commits行数
    commit_count=$(echo "$commits" | wc -l)
    # 将commits内容用join拼接，分隔符为空格
    joined_commits=$(echo "$commits" | paste -sd ' ')

    # 进入对应路径下的目录，执行git format-patch命令
    echo "Enter $repo_dir"
    (cd $current_dir/$repo_dir && eval "git format-patch -$commit_count $joined_commits -o $script_dir/$repo_dir")
done <<< $(cat "$script_dir/patches.json" | jq -c '.[]')