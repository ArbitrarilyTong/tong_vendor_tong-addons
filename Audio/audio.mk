LOCAL_PATH := ArbitrarilyTong/Audio/sounds

# Copy audio files to output directory
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/notifications/ogg/Argon.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Argon.ogg \
    $(LOCAL_PATH)/notifications/ogg/ATOMS.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/ATOMS.ogg \
    $(LOCAL_PATH)/notifications/ogg/CRYSTALS.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/CRYSTALS.ogg \
    $(LOCAL_PATH)/notifications/ogg/FLUX.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/FLUX.ogg \
    $(LOCAL_PATH)/notifications/ogg/GLITTER.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/GLITTER.ogg \
    $(LOCAL_PATH)/ringtones/ogg/ArbitrarilyTongTune.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/ArbitrarilyTongTune.ogg

# Default notification/alarm sounds
# PRODUCT_PRODUCT_PROPERTIES += ro.config.notification_sound=FLUX.ogg

# Default ringtone
# PRODUCT_PRODUCT_PROPERTIES += ro.config.ringtone=ArbitrarilyTongTune.ogg

# Override the default notification/alarm sounds and ringtone
PRODUCT_PROPERTY_OVERRIDES := \
    ro.config.notification_sound=FLUX.ogg \
    ro.config.ringtone=ArbitrarilyTongTune.ogg